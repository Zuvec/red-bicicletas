var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

describe('Bicicleta API', () => {

  beforeEach( (done) => {
    Bicicleta.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
  });

  afterEach( (done) => {
    Bicicleta.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
  });


  describe('GET BICICLETAS /', () => {
    it('Status 200, sin bicis', (done) => {
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0);
        done();
      });
    });

    it('Status 200, agrega una bici', (done) => {
      var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
      Bicicleta.add(aBici, function(err, newBici) {
        if (err) console.log(err);
        Bicicleta.allBicis(function(err, bicis) {
          expect(bicis.length).toBe(1);
          expect(bicis[0].code).toEqual(aBici.code);
          done();
        });
      });
    });

    request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
      expect(response.statusCode).toBe(200);
    });

  });

  describe ('POST BICICLETAS /create', () => {
    it('Status 200', (done) => {
      var headers = {'content-type' : 'application/json'};
      var aBici = { "id": 10, "color": "rojo", "modelo": "urbana", "lat":-34, "lng": -54};
      console.log('Bici: ' + JSON.stringify(aBici));
      request.post({
          headers: headers,
          url: 'http://localhost:3000/api/bicicletas/create',
          body: JSON.stringify(aBici)
      }, function(error, response, body) {

          expect(response.statusCode).toBe(200);
          var bici = JSON.parse(body).bicicletas;
          expect(bici.color).toBe(aBici.color);
          expect(bici.modelo).toBe(aBici.modelo);
          expect(bici.ubicacion[0]).toBe(aBici.lat);
          expect(bici.ubicacion[1]).toBe(aBici.lng);
          done();
        });
    });
  });

  
  describe ('POST BICICLETAS /delete', () => {
      it('Status 204', (done) => {
          var headers = {'content-type' : 'application/json'};
          var a = new Bicicleta({code: 10, color: 'rojo', modelo: 'urbana', ubicacion: [-34, -54]});
          var aBiciId = { "id": a.code };
          Bicicleta.add (a);
          
          expect(Bicicleta.allBicis.length).toBe(1);

          request.post({
              headers: headers,
              url: 'http://localhost:3000/api/bicicletas/delete',
              body: JSON.stringify(aBiciId)
          }, function(error, response, body) {
              expect(response.statusCode).toBe(204);
              Bicicleta.allBicis(function(err, doc) {
                  expect(doc.length).toBe(0);
                  done();
              });
          });
      });
  });

  describe ('POST BICICLETAS /update', () => {
      it('Status 200', (done) => {
          var headers = {'content-type' : 'application/json'};
          var a = new Bicicleta({code: 10, color: 'rojo', modelo: 'urbana', ubicacion: [-34, -54]});
          Bicicleta.add (a, function() {
              var headers = {'content-type' : 'application/json'};
              var updatedBici = { "id": a.code, "color": "verde", "modelo": "montaña", "lat":-33, "lng": -55};
              request.post({
                  headers: headers,
                  url: 'http://localhost:3000/api/bicicletas/update',
                  body: JSON.stringify(updatedBici)
              }, function(error, response, body) {
                  expect(response.statusCode).toBe(200);
                  var foundBici = Bicicleta.findByCode(10, function(err, doc) {
                      expect(doc.code).toBe(10);
                      expect(doc.color).toBe(updatedBici.color);
                      expect(doc.modelo).toBe(updatedBici.modelo);
                      expect(doc.ubicacion[0]).toBe(updatedBici.lat);
                      expect(doc.ubicacion[1]).toBe(updatedBici.lng);
                      done();
                  });
              });
          });
      });
  });

});

