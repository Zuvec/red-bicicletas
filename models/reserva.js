var moongose = require('mongoose');
var moment = require('moment');
var Schema = moongose.Schema;

var reservaSchema = new Schema({
  desde: Date,
  hasta: Date,
  bicicleta: { type: moongose.Schema.Types.ObjectId, ref: 'Bicicleta'},
  usuario: { type: moongose.Schema.Types.ObjectId, ref: 'Usuario'},
});

reservaSchema.methods.diasDeReserva = function() {
  return moment(this.hasta).diff(moment(this.desde), 'days') +1;
}

module.exports = moongose.model('Reserva', reservaSchema);