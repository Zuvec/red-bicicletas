const moongose = require('mongoose');
const Schema = moongose.Schema;

const TokenSchema = new Schema({
  _userId: {type: moongose.Schema.Types.ObjectId, required: true, ref: 'Usuario'},
  token: {type: String, required: true},
  createdAt: { type: Date, required: true, default: Date.now, exprires: 43200}
});

module.exports = moongose.model('Token', TokenSchema);